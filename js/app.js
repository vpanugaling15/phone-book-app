(function(){
	angular.module("phoneBook", ["ui.router"])
	  .config(
	    [
	      '$stateProvider',
	      '$urlRouterProvider',
	      function($stateProvider, $urlRouterProvider) {
	      	$urlRouterProvider.otherwise('/');
	      	$stateProvider
				.state('home', {
					url: "/",
					controller: 'PhonebookCtrl'
				})
				.state('create', {
					url: "/create",
					templateUrl: '/builds/development/partials/modal-create.html',
					controller: 'CreateContactCtrl'
				})
				.state('view', {
					url: "/view?id",
					templateUrl: '/builds/development/partials/modal-view.html',
					controller: 'ViewContactCtrl'
				})
				.state('update', {
					url: "/update?id",
					templateUrl: '/builds/development/partials/modal-update.html',
					controller: 'UpdateContactCtrl'
				})
				.state('delete', {
					url: "/delete?id",
					controller: 'DeleteContactCtrl'
				})
	      }
	    ]
	  );
 
	var app = angular.module("phoneBook")
	
	app.controller('PhonebookCtrl', ['$scope', 'phonebook', function($scope, phonebook) {
		// Fix for bootstrap modal duplicate overlays.
		jQuery('.modal-backdrop').remove()

		$scope.updateContacts = function(obj) {
			$scope.contacts = obj;
		}

		if(localStorage.getItem('contacts')) {
			$scope.contacts = phonebook.getContacts();
		}
	}]);

	// View Contact
	app.controller('ViewContactCtrl', ['$scope', '$stateParams', 'phonebook', function($scope, $stateParams, phonebook) {
		jQuery('#viewContact').modal('show') 

		var selectedContactIndex = phonebook.findContactIndex($stateParams.id);
		var contacts = phonebook.getContacts();

		// Move the selected contact object to contact scope to be displayed in view.
		$scope.contact = contacts[selectedContactIndex];
	}]);

	// New/Create Contact
	app.controller('CreateContactCtrl', ['$scope', '$location', 'phonebook', function($scope, $location, phonebook) {
		jQuery('#createContact').modal('show') 

		$scope.createContact = function() {
			if(localStorage.getItem('contacts')) {
				$scope.contacts = phonebook.getContacts();
			}

			try {
				phonebook.saveContact($scope.contact);

				// Update contacts in view.
				$scope.$parent.updateContacts(phonebook.getContacts());
				$location.url($location.path('/'));

				jQuery('#createContact').modal('hide') 
			} catch (err) {
				$scope.message = "Snap! Could not save contact. Please try again later.";
				$scope.alertType = "danger";
			}
		};

		$scope.validatePhone = function() {
			if( ! phonebook.validatePhone($scope.contact.phoneNumber)) {
				$scope.message = "Invalid phone number, please input a correct one.";
				$scope.alertType = "danger";
			} else {
				$scope.message = "";
				$scope.alertType = "";
			}
		}
	}]);

	// Update Contact
	app.controller('UpdateContactCtrl', ['$scope', '$stateParams', '$location', 'phonebook', function($scope, $stateParams, $location, phonebook) {
		jQuery('#updateContact').modal('show') 

		var selectedContactIndex = phonebook.findContactIndex($stateParams.id);
		var contacts = phonebook.getContacts();
		var selectedContact = contacts[selectedContactIndex];

		if(selectedContact) {
			$scope.contact = {};

			// Populate values in the form fields.
			angular.forEach(selectedContact, function (value, key) {
				$scope.contact[key] = selectedContact[key]
			})

			$scope.updateContact = function() {
				try {
					// Map the updated contact to the contact list object.
					angular.forEach($scope.contact, function (value, key) {
						contacts[selectedContactIndex][key] = $scope.contact[key]
					})
					var updatedContacts = phonebook.getContactsJSON(contacts);

					// Store the updated contact list object to localStorage.
					localStorage.setItem('contacts', updatedContacts);

					// Update the table view with the updated list of contact.
					$scope.$parent.updateContacts(phonebook.getContacts());
					$location.url($location.path('/'));
				} catch (err) {
					$scope.message = "Snap! Could not save contact. Please try again later.";
					$scope.alertType = "danger";
				}
			}

			$scope.validatePhone = function() {
				if( ! phonebook.validatePhone($scope.contact.phoneNumber)) {
					$scope.message = "Invalid phone number, please input a correct one.";
					$scope.alertType = "danger";
				} else {
					$scope.message = "";
					$scope.alertType = "";
				}
			}
		} else {
			// Redirect if the ID or selected contact is null.
			$location.url($location.path('/'));
		}
		
	}]);
	
	// Delete Contact
	app.controller('DeleteContactCtrl', ['$scope', '$stateParams', '$location', 'phonebook', function($scope, $stateParams, $location, phonebook) {
		if(confirm("Are you sure you want to delete this contact?")) {
			try {
				var selectedContactIndex = phonebook.findContactIndex($stateParams.id);
				var contacts = phonebook.getContacts();

				// Delete a contact in contacts object.
				contacts.splice(selectedContactIndex, 1);
				
				var updatedContacts = phonebook.getContactsJSON(contacts);

				// Store the updated contact list object to localStorage.
				localStorage.setItem('contacts', updatedContacts);

				// Update the table view with the updated list of contact.
				$scope.$parent.updateContacts(phonebook.getContacts());
				$location.url($location.path('/'));
			}  catch (err) {
				alert("Snap! Could not delete contact. Please try again later.");
			}
		} else {
			$location.url($location.path('/'));
		}
	}]);
}());