(function(){
	var contact = function() {

		var getId = function() {
			return Date.now().toString(36).substr(2, 9);
		};

		var getContacts = function() {
			return JSON.parse('[' + localStorage.getItem('contacts') + ']');
		};

		var getContactsJSON = function(contacts) {
			return JSON.stringify(contacts).substr(1, JSON.stringify(contacts).length - 2)
		};

		var saveContact = function (contactObj) {
			contactObj.id = getId();

			if(localStorage.contacts) {
				var tmpContacts = [];

			    tmpContacts.push(localStorage.getItem('contacts'));
			    tmpContacts.push(JSON.stringify(contactObj));
				
				localStorage.setItem('contacts', tmpContacts);
			} else {
				localStorage.setItem('contacts', JSON.stringify(contactObj));
			}
		}

		var validatePhone = function (phoneNumber) {
			var expression = /^\(?([0-9]{4})\)?[-]?([0-9]{7})$/;

			if( ! phoneNumber.match(expression)) {
				return false;
			} else {
				return true;
			}
		}

		var findContactIndex = function (id) {
			var contactsObj = getContacts();
			var selectedContactIndex;

			for (var i = 0; i < contactsObj.length; i++) {
			    angular.forEach(contactsObj[i], function(value, key) {
			       if(value == id) {
			         selectedContactIndex = i;
			       }
				});
			}

			return selectedContactIndex;
		}

		return {
			getId: getId,
			getContacts: getContacts,
			saveContact: saveContact,
			validatePhone: validatePhone,
			findContactIndex: findContactIndex,
			getContactsJSON: getContactsJSON
		}
	}

	var module = angular.module("phoneBook");
	module.factory('phonebook', contact);
}());