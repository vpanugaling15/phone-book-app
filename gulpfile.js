'use strict';

var coffee_script = require('coffee-script'),
	connect = require('connect'),
	express = require('express'),
	express_winston = require('express-winston'),
	gulp = require('gulp'),	
	coffee = require('gulp-coffee'),
	concat = require('gulp-concat'),
	_if = require('gulp-if'),
	if_else = require('gulp-if-else'),
	jade = require('gulp-jade'),
	livereload = require('gulp-livereload'),
	minify_css = require('gulp-minify-css'),
	replace = require('gulp-replace'),
	stylus = require('gulp-stylus'),
	uglify = require('gulp-uglify'),
	winston = require('winston'),
	args  = require('yargs').argv;

// Jade Task
gulp.task('jade', function () {
	return gulp.src('templates/**/*.jade')
		.pipe(jade()) // pip to jade plugin
		.pipe(gulp.dest('builds/development')); // tell gulp our output folder
		
});

// Stylus Task
gulp.task('stylus', function () {
	return gulp.src('styles/**/*.styl')
		.pipe(stylus()) 
		.pipe(gulp.dest('builds/development/css')); // tell gulp our output folder
		
});

// Minify Javascript
gulp.task('scripts', function() {
	gulp.src('js/**/*.js')
		.pipe(uglify())
		.pipe(gulp.dest('builds/development/js'))
});


// Server
gulp.task('server', function () {
  var app = express();
  app.use(require('connect')());
  app.use("/bower", express.static(__dirname + '/bower_components'));
  app.use("/node", express.static(__dirname + '/node_modules'));
  app.use("/js", express.static(__dirname + '/js'));
  app.use("/builds", express.static(__dirname + '/builds'));
  app.use(express.static(__dirname + '/builds/development/'));
  app.listen(4000, '0.0.0.0');
});

gulp.task('watch', function() {
  //livereload.listen();
  gulp.watch('templates/**/*.jade', ["jade"]);
  gulp.watch('js/**/*.js', ["scripts"]);
  gulp.watch('styles/**/*.styl', ["stylus"]);
});

// Define default task
gulp.task("default", ["jade", "stylus", "server", "watch"], function(){
  gulp.watch([
    'templates/**/*.jade',
  ]).on('change', function(event) {
    //livereload.changed();
    console.log('File', event.path, 'was', event.type);
    console.log('LiveReload is triggered');
  });
});