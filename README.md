# README #

A simple phone book app built with [AngularJS](https://angularjs.org/)

### PhoneBook App ###

* One page web app that will perform CRUD operation for managing contacts.
* Version 1.0

### How do I get set up? ###

* You must have installed [Node.JS](https://nodejs.org/en/)
* Install [Bower](https://bower.io/) package manager.
* Install dependencies from node and bower files dependency list.
* Run gulp task 'gulp'
* Access the app using this URL (http://localhost:4000)
